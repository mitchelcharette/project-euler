//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

public class Euler5_1 {

	public static int solve(int x) {
		int w = 1;
		for(int i = 1; i <= x; i++) {
			if(w % i != 0) {
				i = 1;
				w++;
			}
		}
		return w;
	}
	
	public static void main(String[] args) {
		System.out.println(solve(20));
	}
}
