
//If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

//Find the sum of all the multiples of 3 or 5 below 1000.

public class Euler1 {
	
		
	public static int solve(int x, int y, int max) {
		int sum = 0;
		for(int w = 0; w < max; w++) {
			if(w % x == 0 || w % y == 0) {
				sum = sum + w;
			}
		}
		return sum;
	}
	

public static void main(String[] args)
{
	System.out.println(solve(3, 5, 1000)); //You can use any numbers here
	}


}
