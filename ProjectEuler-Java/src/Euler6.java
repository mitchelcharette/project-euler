//The sum of the squares of the first ten natural numbers is 385


//The square of the sum of the first ten natural numbers is 3025

//Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 2640
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
public class Euler6 {

	
	public static int solve(int x) {
		int total = 0;
		int sum = 0;
		for(int i = 1; i <= x; i++) {
			int k = i*i;
			sum += k;
		}
		for(int j = 1; j <= x; j++) {
			total += j;
		}
		total = total*total;
		
		int answer = total - sum;
		
		return answer;
		}
	
	
	
	public static void main(String[] args) {
		System.out.println(solve(100));
	}
}