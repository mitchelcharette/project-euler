//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


public class Euler5 {
	
	public static int solve() {
		int solution = 1;
		boolean test = true;
		while(test) {
			if(solution % 2 == 0 && solution % 3 == 0 && solution % 4 == 0 && solution % 5 == 0 
					&& solution % 6 == 0 && solution % 7 == 0 && solution % 8 == 0 && solution % 9 == 0 
					&& solution % 10 == 0 && solution % 11 == 0 && solution % 12 == 0 && solution % 13 == 0 
					&& solution % 14 == 0 && solution % 15 == 0 && solution % 16 == 0 && solution % 17 == 0 
					&& solution % 18 == 0 && solution % 19 == 0 && solution % 20 == 0) {
				test = false;
			}else {
				solution++;
			}
		}
		return solution;
	}

	public static void main(String[] args) {
		System.out.println(solve());
	}
}
