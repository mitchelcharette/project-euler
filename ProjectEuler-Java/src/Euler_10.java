import java.util.ArrayList;

public class Euler_10 {

	
	public static int solve(int x) {
		int answer = 5;
		int prime;
		int prime1;
		ArrayList<Integer>array = new ArrayList<Integer>();
		array.add(2);
		array.add(3);
		x = x - (x % 6);
		for(int i = 1; i <= x/6; i++) {
			prime = 6*i - 1;
			prime1 = 6*i + 1;
			boolean check = false;
				for(int element: array) {
					if(element > x/2) {
						break;
					}
					if(prime % element == 0) {
						check = false;
						break;
					}else {
						check = true;
					}
			}
				if(check) {
					answer += prime;
					array.add(prime);
				}
				check = false;
				for(int element: array) {
					if(element > x/2) {
						break;
							}
					if(prime1 % element == 0) {
						check = false;
						break;
					}else {
						check = true;
							}
			}
				if(check) {
					answer += prime1;
					array.add(prime1);
				}
		}

		return answer;
	}

	
	public static void main(String[] args) {
		System.out.println(solve(1000));
	}
}
