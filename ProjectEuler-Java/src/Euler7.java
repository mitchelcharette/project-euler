import java.util.ArrayList;

//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

//What is the 10001st prime number?


public class Euler7 {

	public static int solve(int x) {
		int answer = 0;
		int prime = 3;
		ArrayList<Integer>array = new ArrayList<Integer>();
		array.add(2);
		for(int i = 0; i < x - 1; i++ ) {
			boolean check = true;
			while(check){
				boolean doubleCheck = false;
				for(int element: array) {
					if(prime % element == 0) {
						doubleCheck = true;
					}else {
						check = false;
					}
					if(doubleCheck) {
						check = true;
					}
			}
			prime += 2;
		}
			array.add(prime-2);
		
	}
		/*for(int element: array) {
			System.out.println(element);
		}
		*/
		answer = array.get(array.size()-1);
		return answer;
	}

	
	public static void main(String[] args) {
		System.out.println(solve(10001));
	}
}
