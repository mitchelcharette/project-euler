/* 
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a^2 + b^2 = c^2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

a = m^2 - n^2, b = 2mn, c =  m^2 + n^2
*/

public class Euler9 {
	
	public static void solve() {
		int a;
		int b;
		int c;
		int l = 0;
		int m = 0;
		int n = 0;
		
		for(int i = 0; i < 1000; i++) {
			for(int j = 0; j < 1000; j++) {
				a = i*i - j*j;
				b = 2*i*j;
				c = i*i + j*j;
				if(a + b + c == 1000) {
					l = a;
					m = b;
					n = c;
				}
			}
		}
		System.out.println("a is " + l + ", b is " + m + ", c is " + n);
		System.out.println("The product is " + l*m*n);
	}

	public static void main(String[] args) {
		solve();
	}
}
