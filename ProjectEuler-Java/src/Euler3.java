import java.util.ArrayList;
import java.util.Collections;

//The prime factors of 13195 are 5, 7, 13 and 29.

//What is the largest prime factor of the number 600851475143 ?


public class Euler3 {

	public static double solve(double x) {
		boolean check = true;
		ArrayList<Integer>array = new ArrayList<Integer>();
		int i = 1;
		double factor = x;
		while(check) {
			if(factor == i) {
				array.add(i);
				check = false;
			}else
			if(factor % i == 0) {
				array.add(i);
				factor = factor/i;
				i = 1;
			}
			i++;
		}
		Integer k = Collections.max(array);
		return k;
	}
	
	public static void main(String[] args) {
		double number = 600851475143L;
		System.out.println(solve(number));
	}
}
