import java.util.ArrayList;

//The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

//Find the sum of all the primes below two million.


public class Euler10 {

	public static int solve(int x) {
		int answer = 2;
		int prime = 3;
		ArrayList<Integer>array = new ArrayList<Integer>();
		array.add(2);
		while(prime < x-2) {
			boolean check = true;
			while(check){
				
				for(int element: array) {
					if(element > x/2) {
						break;
					}
					if(prime % element == 0) {
						check = true;
						break;
					}else {
						check = false;
					}
			}
			
			prime += 2;
		}
			
			answer += prime-2;
			array.add(prime-2);
		}
		return answer;
	}

	
	public static void main(String[] args) {
		System.out.println(solve(200000));
	}
}
