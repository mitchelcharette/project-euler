import java.util.ArrayList;
import java.util.Collections;

//A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.

//Find the largest palindrome made from the product of two 3-digit numbers.


public class Euler4 {
	
	public static int[] solve() {
		
		
		ArrayList<Integer>array = new ArrayList<Integer>();
		ArrayList<Integer>arrayX = new ArrayList<Integer>();
		ArrayList<Integer>arrayY = new ArrayList<Integer>();
	
		int x = 999;
		int y = 999;
		
		for(int i = 0; i < 999; i++) {
			y = 999;
			for(int j = 0; j < 999; j++) {
				String k = Integer.toString(x*y);
				StringBuilder sb = new StringBuilder(k);
				String l = sb.reverse().toString();
				if(k.equals(l)) {
					arrayX.add(x);
					arrayY.add(y);
					array.add(x*y);
				}
				y--;
			}
			x--;
		}
		int m = array.indexOf(Collections.max(array));
		int[] n;
		n = new int[3];
		n[0]= array.get(m);
		n[1]= arrayX.get(m);
		n[2]= arrayY.get(m);
		/*
		for(int element: array) {
			System.out.println(element);
		}
		*/
		return n;
	}

	
	public static void main(String[] args) {
		
		for (int element: solve()) { 
			System.out.println(element); 
			}
	}
}
